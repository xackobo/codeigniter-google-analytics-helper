<?php

	#
	#	Google Analytics Code
	#				

		if ( ! function_exists('google_analytics'))
		{
			function google_analytics($account, $USER_ID=null)
			{
				$code = "<script>";
  				$code.= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){";
  				$code.= "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),";
  				$code.= "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)";
  				$code.= "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');";

  				$code.= "ga('create', '".$account."', 'auto');";
  				if(isset($USER_ID))$code.= "ga('set', '&uid', '".$USER_ID."');";// Set the user ID using signed-in user_id.
  				$code.= "ga('send', 'pageview');";


				$code.= "</script>";
				
				return $code;
			}
		}
		
	