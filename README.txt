Requirements:
 - You must have a Google Analytics account and have the project registered there
 - Get the UA code from Google Analytics

Install:

1- Put the google_analytics_helper.php file in /applications/helpers/ directory
2- On your template insert this code just before page body colsing tag (</body>):
	
	<?php if(ENVIRONMENT=='production' or true) echo google_analytics('UA-xxxxxxxx-x', [[USER_ID]]); ?>

Where:
UA-xxxxxxxx-x: is the Google Analytics code
[[USER_ID]]: is the user id on your application. This parameter is optional.